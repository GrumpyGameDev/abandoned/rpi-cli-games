#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>

#define BUFFER_SIZE (160)
#define DELIMITERS (" \n\t")

#define COMMAND_QUIT ("QUIT")
#define COMMAND_HELP ("HELP")
#define COMMAND_DEAL ("DEAL")
#define COMMAND_REDEAL ("REDEAL")
#define COMMAND_BET ("BET")
#define COMMAND_BANK ("BANK")

typedef enum
{
	STATE_NO_GAME,
	STATE_DEAL
} GAME_STATE;

void strupr(char* s)
{
	for(char* c = s; *c; ++c)
	{
		*c = toupper(*c);
	}
}

void show_help()
{
	printf("\nHelp:\n");
	printf("\tQUIT\n");
	printf("\tHELP\n");
	printf("\tDEAL\n");
	printf("\tREDEAL\n");
	printf("\tBET\n");
	printf("\tBANK\n");
}

typedef enum
{
    CARD_DEUCE,
    CARD_THREE,
    CARD_FOUR,
    CARD_FIVE,
    CARD_SIX,
    CARD_SEVEN,
    CARD_EIGHT,
    CARD_NINE,
    CARD_TEN,
    CARD_JACK,
    CARD_QUEEN,
    CARD_KING,
    CARD_ACE,
    CARD_COUNT

} CARD_RANK;

const char* card_names[CARD_COUNT] =
{
    "Deuce",
    "Three",
    "Four",
    "Five",
    "Six",
    "Seven",
    "Eight",
    "Nine",
    "Ten",
    "Jack",
    "Queen",
    "King",
    "Ace"
};

int main(int argc, char* argv[])
{
	srand(time(0));
	char buffer[BUFFER_SIZE];
	printf("**Acey Deucey**\n");
	int done = 0;
    int money = 100;
    GAME_STATE state = STATE_NO_GAME;
    CARD_RANK cards[3];
	char* next_token;
	while(!done)
	{
		printf("\n>");
		char* line = fgets(buffer,BUFFER_SIZE,stdin);
		strupr(line);
		char* token = strtok_r(line, DELIMITERS, &next_token);
		if(token)
		{
			if(strcmp(token, COMMAND_QUIT)==0)
			{
				done = 1;
			}
            else if(strcmp(token, COMMAND_HELP)==0)
			{
				show_help();
			}
            else if(strcmp(token, COMMAND_BANK)==0)
			{
				printf("\nYou have $%d\n",money);
			}
            else if(strcmp(token, COMMAND_DEAL)==0)
			{
				if(state==STATE_DEAL)
                {
                    printf("\nThis round has already been dealt. If you do not care to bet, use REDEAL.\n");
                }
                else
                {
                    cards[0]=rand()%CARD_COUNT;
                    cards[1]=rand()%CARD_COUNT;
                    cards[2]=rand()%CARD_COUNT;
                    state=STATE_DEAL;
                    printf("\nHere are the cards: %s %s\n",card_names[cards[0]],card_names[cards[1]]);
                }
			}
            else if(strcmp(token, COMMAND_BET)==0)
			{
                token=strtok_r(0,DELIMITERS,&next_token);
                if(token)
                {
                    int bet = atoi(token);
                    if(bet<1 || bet>money)
                    {
                        printf("\nPlease make a valid bet.\n");
                    }
                    else
                    {
                        state=STATE_NO_GAME;
                        printf("\nThe final card: %s\n",card_names[cards[2]]);
                        if((cards[0]>cards[1])?(cards[0]>cards[2] && cards[2]>cards[1]):(cards[2]<cards[2] && cards[2]>cards[1]))
                        {
                            money+=bet;
                            printf("You won! You now have $%d\n",money);
                        }
                        else
                        {
                            money-=bet;
                            printf("You lost! You now have $%d\n",money);
                            if(money<1)
                            {
                                printf("You are out of money! Get lost!\n");
                                done=1;
                            }
                        }
                    }
                }
                else
                {
                }
			}
            else if(strcmp(token, COMMAND_REDEAL)==0)
			{
                cards[0]=rand()%CARD_COUNT;
                cards[1]=rand()%CARD_COUNT;
                cards[2]=rand()%CARD_COUNT;
                state=STATE_DEAL;
                printf("\nHere are the new cards: %s %s\n",card_names[cards[0]],card_names[cards[1]]);
			}
			else
			{
				printf("\nInvalid command. Try HELP.\n");
			}
		}
		else
		{
			printf("\nInvalid command. Try HELP.\n");
		}
	}
	return 0;
}
