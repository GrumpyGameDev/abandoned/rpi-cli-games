#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>

#define BUFFER_SIZE (160)
#define DELIMITERS (" \n\t")

#define COMMAND_QUIT ("QUIT")
#define COMMAND_HELP ("HELP")
#define COMMAND_NEW ("NEW")
#define COMMAND_GUESS ("GUESS")
#define COMMAND_SCORE ("SCORE")

void strupr(char* s)
{
	for(char* c = s; *c; ++c)
	{
		*c = toupper(*c);
	}
}

void show_help()
{
	printf("\nHelp:\n");
	printf("\tQUIT\n");
	printf("\tHELP\n");
	printf("\tNEW\n");
	printf("\tGUESS [n]\n");
	printf("\tSCORE\n");
}

int main(int argc, char* argv[])
{
	srand(time(0));
	char buffer[BUFFER_SIZE];
	int number = rand() % 100 + 1;
	int guesses = 0;
	int correct = 0;
	printf("**Guess My Number**\n");
	int done = 0;
	char* next_token;
	while(!done)
	{
		printf("\n>");
		char* line = fgets(buffer,BUFFER_SIZE,stdin);
		strupr(line);
		char* token = strtok_r(line, DELIMITERS, &next_token);
		if(token)
		{
			if(strcmp(token, COMMAND_QUIT)==0)
			{
				done = 1;
			}
			else if(strcmp(token, COMMAND_NEW)==0)
			{
				printf("\nI have selected a new number.\n");
				number = rand() %100 + 1;
				guesses = 0;
				correct = 0;
			}
			else if(strcmp(token, COMMAND_GUESS)==0)
			{
				if(correct)
				{
					printf("\nYou already guessed the number correctly. Why not start a new game?\n");
				}
				else
				{

					token = strtok_r(0,DELIMITERS,&next_token);
					if(token)
					{
						int guess = atoi(token);
						if(guess<1 || guess>100)
						{
							printf("\nPlease guess a number between 1 and 100 inclusive.\n");
						}
						else
						{
							++guesses;
							if(guess==number)
							{
								correct=1;
								printf("\nCorrect! You guessed it after %d guesses!\n", guesses);
							}
							else if(guess>number)
							{
								printf("\nToo high.\n");
							}
							else
							{
								printf("\nToo low.\n");
							}
						}
					}
					else
					{
						printf("\nGUESS [n]\n");
					}
				}
			}
			else if(strcmp(token, COMMAND_SCORE)==0)
			{
				printf("\nYou have made %d guesses so far.\n", guesses);
			}
			else if(strcmp(token, COMMAND_HELP)==0)
			{
				show_help();
			}
			else
			{
				printf("\nInvalid command. Try HELP.\n");
			}
		}
	}
	return 0;
}
