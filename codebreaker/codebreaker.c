#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>

#define BUFFER_SIZE (160)
#define DELIMITERS (" \n\t")

#define COMMAND_QUIT ("QUIT")
#define COMMAND_HELP ("HELP")
#define COMMAND_NEW ("NEW")
#define COMMAND_GUESS ("GUESS")
#define COMMAND_SCORE ("SCORE")

void strupr(char* s)
{
	for(char* c = s; *c; ++c)
	{
		*c = toupper(*c);
	}
}

void show_help()
{
	printf("\nHelp:\n");
	printf("\tQUIT\n");
	printf("\tHELP\n");
	printf("\tNEW\n");
	printf("\tGUESS [n]\n");
	printf("\tSCORE\n");
}

char code[5];
int digits[10];

void generate_code()
{
    int number = rand() % 10000;
    sprintf(code,"%04d",number);
    for(int digit=0;digit<10;++digit)
    {
        digits[digit]=0;
    }
    for(char* c=code;*c;++c)
    {
        digits[*c-'0']++;
    }
}

int main(int argc, char* argv[])
{
	srand(time(0));
	char buffer[BUFFER_SIZE];
    generate_code();
	int guesses = 0;
	int correct = 0;

	printf("\033[0;31m**Code Breaker**\n\033[0m");
	int done = 0;
	char* next_token;
	while(!done)
	{
		printf("\n>");
		char* line = fgets(buffer,BUFFER_SIZE,stdin);
		strupr(line);
		char* token = strtok_r(line, DELIMITERS, &next_token);
		if(token)
		{
			if(strcmp(token, COMMAND_QUIT)==0)
			{
				done = 1;
			}
			else if(strcmp(token, COMMAND_NEW)==0)
			{
				printf("\nI have selected a new code.\n");
				generate_code();
				guesses = 0;
				correct = 0;
			}
			else if(strcmp(token, COMMAND_GUESS)==0)
			{
				if(correct)
				{
					printf("\nYou already guessed the code correctly. Why not start a new game?\n");
				}
				else
				{

					token = strtok_r(0,DELIMITERS,&next_token);
					if(token)
					{
                        if(strlen(token)==strlen(code))
                        {
                            int valid = 1;
                            int check[10]={0};
                            for(char* c = token;valid && *c;++c)
                            {
                                if(*c<'0' || *c>'9')
                                {
                                    valid=0;
                                }
                                else
                                {
                                    check[*c-'0']++;
                                }
                            }
                            if(valid)
                            {
                                ++guesses;
                                int tally=0;
                                for(int digit=0;digit<10;++digit)
                                {
                                    tally += (digits[digit]<check[digit]) ? (digits[digit]) : (check[digit]);
                                }
                                int exact=0;
                                for(char *c=code,*d=token;*c;++c,++d)
                                {
                                    exact += (*c==*d) ? (1) : (0);
                                }
                                printf("\nCorrect digits:%d\nExactly correct digits:%d\n",tally,exact);
                                if(exact==strlen(code))
                                {
                                    printf("\nYou guessed it correctly in %d guesses.\n",guesses);
                                    correct=1;
                                }
                            }
                            else
                            {
                                printf("\nInvalid code.\n");
                            }
                        }
                        else
                        {
                            printf("\nInvalid code.\n");
                        }
					}
					else
					{
						printf("\nGUESS [n]\n");
					}
				}
			}
			else if(strcmp(token, COMMAND_SCORE)==0)
			{
				printf("\nYou have made %d guesses so far.\n", guesses);
			}
			else if(strcmp(token, COMMAND_HELP)==0)
			{
				show_help();
			}
			else
			{
				printf("\nInvalid command. Try HELP.\n");
			}
		}
	}
	return 0;
}
